import os
import yaml

from  collections import MutableMapping
from compago.plugin import Plugin


class Config(MutableMapping):

    @staticmethod
    def load(path):
        try:
            attrs = yaml.load(open(path))
        except IOError:
            return Config()
        else:
            return Config(**attrs)

    def __init__(self, **kwargs):
        self.attributes = kwargs or {}

    def __getitem__(self, key):
        try:
            return self.attributes[key]

        except KeyError:
            raise Exception('{0} is not configured.'.format(key))

    def keys(self):
        return self.attributes.keys()

    def empty(self):
        return True if self else False

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def __delitem__(self, key):
        del self.__dict__[key]

    def __iter__(self):
        return iter(self.__dict__)

    def __len__(self):
        return len(self.__dict__)

    # The final two methods aren't required, but nice for demo purposes:
    def __str__(self):
        '''returns simple dict representation of the mapping'''
        return str(self.__dict__)

    def __repr__(self):
        '''echoes class, id, & reproducible representation in the REPL'''
        return '{}, D({})'.format(super(D, self).__repr__(), 
                                  self.__dict__)


class ConfigPlugin(Plugin):

    def __init__(self, path=None):
        self.path = path

    def after_application_init(self, application):
        application.add_option('--configfile', dest='cfpath',
                               metavar='PATH',
                               help='The path to the config file.')
        if application.args['cfpath']:
            if not os.path.exists(application.args['cfpath']):
                raise IOError('Config file {0} does not exist!'.format(
                    application.args['cfpath']))
            self.path = application.args['cfpath']
        if not self.path:
            self.path = '{0}.conf'.format(application.name)
        application.config = Config.load(self.path)
