__version__ = (1, 5, 0)

from compago.option import Option
from compago.command import Command, CommandError
from compago.application import Application, ApplicationError
